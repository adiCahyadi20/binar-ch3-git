// #1

let fruits = ["Apple", "Banana", "Papaya", "Grape", "Cherry", "Peach"];
fruits.reverse();
fruits.forEach(function (item) {
  console.log(item);
});
/* 
Output: 
Peach
Cherry
Grape
Papaya
Banana
Apple
*/

// #2

let month = ["January", "February", "July", "August", "September", "October", "November", "December"];

let pisah = month.splice(2);
pisah.unshift("march", "april", "may", "june");
let gabung = month.concat(pisah);

console.log(gabung);

/* 
Output: 
 ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
*/

// #3
const message = "Sampaikan pada Sabrina belajar Javascript sangat menyenangkan";

let slice = message.slice(22);

console.log(slice);

/* 
Output: belajar Javascript menyenangkan
*/

// #4
let replace = slice.replace("b", "B");
console.log(replace);
/* 
Output: Belajar Javascript menyenangkan
*/

/* #5 Buat function untuk menghitung BMI (Body Mass Index)
- BMI = mass / height ** 2 = mass / (height * height) (mass in kg and height in meter)
- Steven weights 78 kg and is 1.69 m tall. Bill weights 92 kg and is 1.95 
m tall
*/
let steven = {
  weight: 78,
  height: 1.69,
};

let bill = {
  weight: 92,
  height: 1.95,
};

function bmi(weight, height) {
  return weight / height ** 2;
}

let bmiSteven = bmi(steven.weight, steven.height);
let bmiBill = bmi(bill.weight, bill.height);
console.log(`BMI steven is ${bmiSteven}`);
console.log(`BMI Bill is ${bmiBill}`);

/* #6 Menghitung BMI dengan if else statement
- John weights 95 kg and is 1.88 m tall. Nash weights 85 kg and is 1.76 
m tall
Output example:
John's BMI (28.3) is higher than Nash's (23.5)
*/
let john = {
  weight: 95,
  height: 188,
};
let nash = {
  weight: 85,
  height: 176,
};

let bmiJohn = bmi(john.weight, john.height);
let bmiNash = bmi(nash.weight, nash.height);

if (bmiJohn > bmiNash) {
  console.log(`John BMI (${bmiJohn}) is higher than Nash'S (${bmiNash})`);
} else {
  console.log(`Nash's BMI (${bmiNash}) is higher than Jhon (${bmiJohn})`);
}

//  #7 Looping

let data = [10, 20, 30, 40, 50];
let total;

/* You code here (you are allowed to reassigned the variable) 
Maybe you can write 3 lines or more
Use for, do while, while for, or forEach
*/

for (let i = 0; i < data.length; i++) {
  total = data[i] * 3;
}

console.log(`Jumlah total = ${total}`);

/* 
Jumlah total = 150
*/
